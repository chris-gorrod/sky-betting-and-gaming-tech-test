# Chris Gorrod - Sky Betting and Gaming Tech Test

This is a [Next.js](https://nextjs.org/) project using [create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Tech Stack

For this task I've used Next JS. I know this is being used in house and have heard a lot about this recently so thought it'd be a good opportunity to give it a go and see how similar/different it is to using React. 

I've set up quite a bit of config for eslint and prettier to try and keep things as consistent as possible. 

The project is as follows:
```
- Yarn
- Next JS with TypeScript
- Jest and React Testing Library
- Scss modules
```

## Running the project

Clone the project using https `git clone https://bitbucket.org/chris-gorrod/sky-betting-and-gaming-tech-test.git`

Using bash, run:

```
- yarn install
- yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

This will also spin up the docker container for the mock api which is available at [http://localhost:8888/](http://localhost:8888/).

## Unit testing

The unit tests are quite sparse and mainly check for the rendering of components and the conditional items within them. Given more time, I'd want to expand this considerably and look at doing some API testing, e2e, integration testing and contract testing. 

Run the unit tests using: 

```bash
- yarn test
```

## Future enhancements

- Add pre-commit git hooks to run linting checks before commiting
- Error handling for requests
- Implementation of a store
- Testing of API requests and responses and contract testing
- Integration and e2e tests
- Possibly consolidate and extend duplicate types e.g. Status
- Implementation of a proper css grid system
- Mobile styling

## Refactor

- Move the initial data request in EventItem to EventList and pass down via props (Currently the EventList component is more or less pointless).
- Check naming conventions (some things could make more sense)
- Create a separate component for the odds type toggle using the toggle component as the parent

## Notes 

### General

- I've never used WebSockets before so the way I've put this together may not be the best practice. I'd want to do a bit more reading of docs, tutorials, blogs etc to get a better understanding of the workings. 
- The testing is very sparse, with more time I'd want to considerably expand this.

### Task Two 

In task two within the project readme it says:

```
Markets similarly contain an array of IDs for outcomes. Use this data to initially show the outcomes for the first ten markets only.
```

The market array for each event is only showing a single ID. Not sure whether this is expected or not but I've built the components with multiple ID's in mind (would probably need double checking or a test adding to make sure multiple are being handled correctly).