module.exports = {
    moduleDirectories: ['node_modules', 'src'],
    projects: [
        {
          displayName: ' dom',
          testEnvironment: 'jsdom',
          testMatch: ['**/*.test.tsx'],
          collectCoverageFrom: ['./**/*.tsx', './**/*.ts', '!**/index.tsx', '!**/styles/**'],
          moduleNameMapper: {
            '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
            '<rootDir>/__mocks__/fileMock.js',
            '\\.(scss|css)$':
            '<rootDir>/__mocks__/styleMock.js',
            '^@components/(.*)$': '<rootDir>/components/$1',
          },
          setupFilesAfterEnv: ['<rootDir>/jest.setup.js'],
          globals: {
            'ts-jest': {
              tsconfig: '<rootDir>/tsconfig.json',
            },
          },
        },
    ]
};
