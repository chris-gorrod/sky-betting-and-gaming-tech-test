import { useState } from 'react';

import EventList from '@components/EventList';
import Toggle from '@components/Toggle';

export default function Home() {
  const [value, setValue] = useState(false);

  return (
    <>
      <section className='container'>
        <Toggle
          textLeft={'Decimal'}
          textRight={'Fractions'}
          isOn={value}
          handleToggle={() => setValue(!value)}
        />

        <EventList 
          isFractionalOdds={value}
        />
      </section>
    </>
  );
}
