import { useEffect, useState } from 'react';
import EventItem from '@components/EventItem';
import { Event } from '../../interfaces/event';
import { eventListProps } from './types';

import styles from './eventList.module.scss';

let ws;

const EventList = (props: eventListProps) => {
  
  const { isFractionalOdds } = props;

  const [liveEvents, setLiveEvents] = useState<Event | null>(null);
  const [showEventsData, setShowEvents] = useState<boolean>(false);

  useEffect(() => {
    ws = new WebSocket('ws://localhost:8889');

    ws.addEventListener('open', () => {
      ws.send(JSON.stringify({ type: 'getLiveEvents', primaryMarkets: true }));
    });

    ws.onmessage = ({ data: eventData }: {data: Event}) => {
      const eventDataResults: Event = JSON.parse(eventData.toString());

      console.log(eventData);

      setLiveEvents(eventDataResults);
    };
    
  }, []);

  useEffect(() => {
    if (liveEvents && liveEvents.type === 'LIVE_EVENTS_DATA' && liveEvents.data.length) {
      setShowEvents(!showEventsData);
    }
  }, [liveEvents]);

  return (
    <>
      <h2 className={styles.eventsType}>Live Football Events</h2>
      <div className={styles.wrapper}>
        {
          showEventsData && (
            liveEvents.data.map((item) => (
              <EventItem 
                key={item.eventId} 
                eventDetails={item}
                ws={ws}
                isFractionalOdds={isFractionalOdds}
              />
            ))
          )
        }
      </div>
    </>
  );
};

export default EventList;