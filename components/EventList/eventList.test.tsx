import { render, screen } from "@testing-library/react";
import EventList from ".";

describe("When the event item is passed the event data required", () => {

    beforeEach(async () => {
        render(<EventList isFractionalOdds={false} />);
    });

    it("should render the heading", () => {
        expect(screen.getByText('Live Football Events')).toBeInTheDocument();
    });
});