import { useState, useEffect } from 'react';
import MarketItem from '@components/Market';
import OutcomeItem from '@components/Outcome';
import Button from '@components/Button';
import { Market, MarketDetails, Outcome, OutcomeDetails } from '@interfaces/index';
import { EventItemProps } from './types';
import styles from './eventItem.module.scss';

const EventItem = (props: EventItemProps): JSX.Element => {
  const { eventDetails, ws, isFractionalOdds } = props;

  const { eventId, name, status, markets, startTime } = eventDetails;

  const [marketData, setMarketData] = useState<MarketDetails[]>([]);
  const [showMarketData, setShowMarketData] = useState<boolean>(false);
  const [outcomeData, setOutcomeData] = useState<OutcomeDetails[]>([]);

  const matchStartTime = new Date(Date.parse(startTime)).toUTCString();

  const handleClick = async () => {
    const marketIds = markets;

    marketIds.forEach((marketId) => {
      ws.send(JSON.stringify({ type: 'getMarket', id: marketId }));

      ws.onmessage = ({ data: market }: {data: MarketDetails[]}) => {
        const marketDataResults: Market = JSON.parse(market.toString());

        const { data } = marketDataResults;

        const checkMarketIds = marketData.some((el) => el.marketId === data.marketId);

        if (!checkMarketIds) {
          setMarketData(prevData => [...prevData, data]);
        }
      };
    });

    setShowMarketData(!showMarketData);
  };

  // Once marketData is updated, fetch outcomes related to it
  useEffect(() => {
    if (marketData) {
      
      const outcomeIds = marketData.flatMap(outcome => outcome.outcomes);

      outcomeIds.forEach((outcome) => {
        ws.send(JSON.stringify({ type: 'getOutcome', id: outcome }));

        ws.onmessage = ({ data: getOutcomeResult }) => {
          const outcomeResult: Outcome = JSON.parse(getOutcomeResult.toString());
          const { data } = outcomeResult;
          
          const checkDataExists = outcomeData.some((el) => el.outcomeId === data.outcomeId);

          if (!checkDataExists) {
            setOutcomeData(prevData => [...prevData, data]);
          } 
        };
      });
    }
  }, [marketData]);


  // Refactor this later - it could potetially cause a rendering loop with the setOutcomData above
  useEffect(() => {
    const sortedDataByDisplayOrder = outcomeData.sort((a, b) => {
      return a.displayOrder - b.displayOrder;
    });

    setOutcomeData(sortedDataByDisplayOrder);
  }, [outcomeData]);
  
  return (
    <div className={styles.eventWrapper}>
      <div className={styles.event} key={eventId}>
        <h3 className={styles.event__name}>{name}</h3>
        <p className={styles.event__active}>{status.active ? 'LIVE' : null}</p>
        <Button buttonText='More details' onClick={handleClick} />
      </div>

      { matchStartTime && (
        <div className={styles.eventDetails}>
          <p>Start time: {matchStartTime}</p>
        </div>
      )}

      {showMarketData && marketData && (
        <>
          <section>
            {marketData && (
              marketData.map ((market) => (
                <MarketItem
                  key={market.marketId}
                  marketDetails={market}
                />
              ))
            )}

            <div className={styles.outcome}>
              {outcomeData && (
                outcomeData.map ((outcome) => (
                  <OutcomeItem
                    key={outcome.outcomeId}
                    outcomeDetails={outcome}
                    isFractionalOdds={isFractionalOdds}
                  />
                ))
              )}
            </div>
          </section>
        </>
      )}
    </div>
  );
};

export default EventItem;