import { render, screen } from "@testing-library/react";
import EventItem from ".";

const mockEvent = {
    eventId: 1234,
    name: 'Mock Event',
    displayOrder: 1,
    sort: '100',
    linkedEventId: 1234,
    classId: 1,
    className: 'mockClassName',
    typeId: 1234,
    typeName: 'Mock Type Name',
    linkedEventTypeId: 5,
    linkedEventTypeName: 'Mock Linked Event Type Name',
    startTime: '2017-09-19T11:35:23.000Z',
    scores: {
        home: 3,
        away: 2,
    },
    competitors: [{
        name: 'Mock Competitor',
        position: '1',
    }],
    status: {
        active: true,
        started: true,
        live: true,
        resulted: false,
        finished: false,
        cashoutable: false,
        displayable: true,
        suspended: false,
        requestabet: false,
    },
    boostCount: 0,
    superBoostCount: 0,
    markets: [1234],
}

describe("When the event item is passed the event data required", () => {

    beforeEach(async () => {
        render(<EventItem eventDetails={mockEvent} ws={null} isFractionalOdds={false} />);
    });

    it("should render the event title", async () => {
        expect(screen.getByText('Mock Event')).toBeInTheDocument();
    });

    it("should render a button to get more market data", async () => {
        expect(screen.getByRole('button')).toBeInTheDocument();
    });

    it("should display a match start time", async () => {
        expect(screen.getByText('Start time: Tue, 19 Sep 2017 11:35:23 GMT')).toBeInTheDocument();
    });
});