import { EventDetails } from '@interfaces/index';

export interface EventItemProps {
    eventDetails: EventDetails;
    ws: WebSocket;
    isFractionalOdds: boolean;
}
