import { render, screen } from "@testing-library/react";
import Button from ".";

const mockButtonClick = jest.fn();

describe('When the button is supplied the required props', () => {
    beforeEach(async () => {
        render(<Button onClick={mockButtonClick} buttonText='Mock Button' />);
    });

    it('should render a button containing the title "Mock Button"', () => {
        expect(screen.getByRole('button')).toHaveTextContent('Mock Button');
    });
});