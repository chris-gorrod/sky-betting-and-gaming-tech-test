export interface buttonProps {
    buttonText: string;
    onClick: () => void;
    secondary?: boolean;
    disabled?: boolean;
}