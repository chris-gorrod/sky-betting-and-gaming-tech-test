import { buttonProps } from './types';
import styles from './button.module.scss';

const Button = (props: buttonProps) => {
    const { onClick, buttonText, secondary, disabled } = props;

    return (
        <button
            className={secondary ? `${styles.button} ${styles.button__secondary}` : styles.button}
            onClick={onClick}
            role='button'
            disabled={disabled}
        >
            { buttonText }
        </button>
    );
}

export default Button;