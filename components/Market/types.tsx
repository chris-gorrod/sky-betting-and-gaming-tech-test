import { MarketDetails } from '@interfaces/index';

export interface MarketItemProps {
    marketDetails: MarketDetails;
}
