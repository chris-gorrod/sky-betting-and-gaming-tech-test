import { MarketItemProps } from './types';

const MarketItem = (props: MarketItemProps): JSX.Element => {
  const { marketDetails } = props;
  const { marketId, name, status } = marketDetails;

  return (
    <div key={marketId} >
      <h4>{name}</h4>
      { status.displayable ? 
        null
        :
        <p>No market data to display</p>
      }
    </div>
  );
};

export default MarketItem;