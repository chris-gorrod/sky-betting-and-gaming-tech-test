import { render, screen } from "@testing-library/react";
import Market from ".";
import { MarketDetails } from '@interfaces/index';

const mockMarket: MarketDetails = {
    marketId: 12345,
    eventId: 56789,
    name: 'mock name',
    displayOrder: 1,
    type: 'Mock type',
    status: {
        active: true,
        live: true,
        resulted: false,
        cashoutable: false,
        displayable: true,
        suspended: false,
        noExtraTime: true, 
    },
    liabilities: {
        livePriceLimit: 6,
    },
    spAvail: false,
    outcomes: [1,2,5],
}

describe("When the event item is passed the event data required", () => {

    beforeEach(async () => {
        render(<Market marketDetails={mockMarket} />);
    });

    it("should render the market name", async () => {
        expect(screen.getByText('mock name')).toBeInTheDocument();
    });
});

describe("When the event item is passed the event data with a displayable status of false", () => {
    mockMarket.status.displayable = false;

    beforeEach(async () => {
        render(<Market marketDetails={mockMarket} />);
    });

    it("should display a no data message", async () => {
        expect(screen.getByText('No market data to display')).toBeInTheDocument();
    });
});