export interface ToggleProps {
    textLeft?: string;
    textRight?: string;
    isOn: boolean;
    handleToggle: () => void;
}
