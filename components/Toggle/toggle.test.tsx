import { fireEvent, render, screen } from "@testing-library/react";
import Toggle from ".";

const mockToggleClick = jest.fn();

beforeEach(() => {
    jest.clearAllMocks();
});

describe("When the event item is passed the all props", () => {
    beforeEach(async () => {
        render(
            <Toggle
                textLeft={'Decimal'}
                textRight={'Fractions'}
                isOn={false}
                handleToggle={jest.fn()}
            />
        )
    });

    it('should render a toggle with text to the left', () => {
        expect(screen.getByText('Decimal')).toBeInTheDocument();
    });

    it('should render a toggle with text to the right', () => {
        expect(screen.getByText('Fractions')).toBeInTheDocument();
    });

    it('should have a hidden checkbox in the DOM', () => {
        expect(screen.getByTestId('checkbox')).not.toBeVisible();
    })
});

describe("When the switch is clicked", () => {
    beforeEach(async () => {
        render(
            <Toggle
                textLeft={'Decimal'}
                textRight={'Fractions'}
                isOn={false}
                handleToggle={mockToggleClick}
            />
        )

        await fireEvent.click(screen.getByTestId('label'));
    });

    it('should trigger the handleToggle function', () => {
        expect(mockToggleClick).toHaveBeenCalled();
    });
});

describe("When the switch is on", () => {
    beforeEach(async () => {
        render(
            <Toggle
                textLeft={'Decimal'}
                textRight={'Fractions'}
                isOn={true}
                handleToggle={mockToggleClick}
            />
        )
    });

    it('should have a check in the hidden checkbox', () => {
        expect(screen.getByTestId('checkbox')).toBeChecked();
    });
});

describe("When no text left or right is supplied", () => {
    beforeEach(async () => {
        render(
            <Toggle
                isOn={true}
                handleToggle={mockToggleClick}
            />
        )
    });

    it('should render a switch', async () => {
        expect(screen.getByTestId('label')).toBeInTheDocument();
    });

    it('should not render text left', async () => {
        expect(screen.queryByTestId('textLeft')).not.toBeInTheDocument();
    });

    it('should not render text right', async () => {
        expect(screen.queryByTestId('textRight')).not.toBeInTheDocument();
    });
});