import styles from './toggle.module.scss';
import { ToggleProps } from './types';

const Switch = (props: ToggleProps) => {
  const { isOn, handleToggle, textLeft, textRight } = props;

  return (
    <div className={styles.switch}>
      <input
        checked={isOn}
        onChange={handleToggle}
        className={styles.checkbox}
        id='checkbox'
        type='checkbox'
        data-testid='checkbox'
        hidden
      />

      { textLeft && 
        <span className={styles.textLeft} data-testid='textLeft'
        >
          {textLeft}
        </span>
      }
      
      <label
        className={`${styles.label} ${isOn ? 'active' : ''}`}
        htmlFor='checkbox'
        data-testid='label'
      >
        <span className={styles.button} />
      </label>

      { textRight && 
        <span className={styles.textRight} data-testid='textRight'>
          {textRight}
        </span>
      }
      
    </div>
  );
};

export default Switch;