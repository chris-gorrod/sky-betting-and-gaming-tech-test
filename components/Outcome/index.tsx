import { OutcomeItemProps } from './types';
import styles from './outcome.module.scss';
import Button from '@components/Button';

const OutcomeItem = (props: OutcomeItemProps): JSX.Element => {
  const { outcomeDetails, isFractionalOdds } = props;

  const { outcomeId, name, price, status } = outcomeDetails;
    
  return (
    <>
      {status.displayable && (
        <div className={styles.outcome} key={outcomeId}>
          <p>{name}</p>

          { status.suspended ?
            <Button 
              secondary 
              buttonText='--' 
              onClick={null} 
              disabled
            />
            :
            <Button 
              secondary 
              buttonText={!isFractionalOdds ? price.decimal : `${price.num}/${price.den}`} 
              onClick={null} 
            />
          }
        </div>
      )}
    </>
  );
};

export default OutcomeItem;