import { OutcomeDetails } from '@interfaces/index';

export interface OutcomeItemProps {
    outcomeDetails: OutcomeDetails;
    isFractionalOdds: boolean;
}
