import { render, screen } from "@testing-library/react";
import Outcome from ".";
import { OutcomeDetails } from '@interfaces/index';

afterEach(() => {
    jest.clearAllMocks();
});

let mockOutcome: OutcomeDetails;

mockOutcome = {
    outcomeId: 13257,
    marketId: 45678,
    eventId: 11111,
    name: 'Mock outcome',
    displayOrder: 4,
    result: {
        place: 1,
        result: 'Mock result',
        favourite: false,
    },    
    price: {
        decimal: '1.5',
        num: '5',
        den: '1',
    },
    status: {
        active: true,
        resulted: false,
        cashoutable: true, 
        displayable: true,
        suspended: false, 
        result: 'Mock result',
    }
}

describe("When the event item is passed the event data required", () => {
    beforeEach(async () => {
        render(
            <Outcome 
                outcomeDetails={mockOutcome}
                isFractionalOdds={false}
            />
        );
    });

    it("should render the outcome name", () => {
        expect(screen.getByText('Mock outcome')).toBeInTheDocument();
    });

    it("should render the outcome odds", () => {
        expect(screen.getByRole('button')).toHaveTextContent('1.5');
    })
});

describe("When the event item is passed the event data required and the user has set fractional odds", () => {
    beforeEach(async () => {
        render(
            <Outcome 
                outcomeDetails={mockOutcome}
                isFractionalOdds={true}
            />
        );
    });

    it("should render the outcome name", () => {
        expect(screen.getByText('Mock outcome')).toBeInTheDocument();
    });

    it("should render the outcome odds", () => {
        expect(screen.getByRole('button')).toHaveTextContent('5/1');
    });
});

describe("When the event item is passed the event data required and the suspended status is true", () => {
    beforeEach(async () => {
        mockOutcome.status.suspended = true;

        render(
            <Outcome 
                outcomeDetails={mockOutcome}
                isFractionalOdds={true}
            />
        );
    });

    it("should render the outcome name", () => {
        expect(screen.getByText('Mock outcome')).toBeInTheDocument();
    });

    it("should not render the outcome odds and display a --", () => {
        expect(screen.getByRole('button')).toHaveTextContent('--');
    })

    it("should disable the button", () => {
        expect(screen.getByRole('button')).toBeDisabled();
    })
});