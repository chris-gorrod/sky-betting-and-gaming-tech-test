export interface Outcome {
    type: string;
    data: OutcomeDetails;
}

export interface OutcomeDetails {
    outcomeId: number;
    marketId: number;
    eventId: number;
    name: string;
    displayOrder: number;
    result: Result;
    price: Price;
    status: Status;
}

export interface Result {
    place: number;
    result: string;
    favourite: boolean;
}

export interface Price {
    decimal: string;
    num: string;
    den: string;
}

export interface Status {
    active: boolean;
    resulted: boolean;
    cashoutable: boolean;
    displayable: boolean;
    suspended: boolean;
    result: string;
}
