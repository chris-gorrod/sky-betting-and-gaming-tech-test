export interface Market {
    type: string;
    data: MarketDetails;
}

export interface MarketDetails {
    marketId: number;
    eventId: number;
    name: string;
    displayOrder: number;
    type: string;
    status: MarketStatus;
    liabilities: Liabilities;
    spAvail: boolean;
    outcomes?: number[];
}

export interface MarketStatus {
    active: boolean;
    resulted: boolean;
    cashoutable: boolean;
    displayable: boolean;
    suspended: boolean;
    noExtraTime: boolean;
    live: boolean;
}

export interface Liabilities {
    livePriceLimit: number;
}
