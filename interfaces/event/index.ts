export interface Event {
  type: string;
  data?: EventDetails[];
}

export interface EventDetails {
  eventId: number;
  name: string;
  displayOrder: number;
  sort: string;
  linkedEventId?: number;
  classId: number;
  className: string;
  typeId: number;
  typeName: string;
  linkedEventTypeId?: number;
  linkedEventTypeName?: string;
  startTime: string;
  scores: Scores;
  competitors?: CompetitorsEntity[];
  status: EventStatus;
  boostCount: number;
  superBoostCount: number;
  markets?: number[];
}

export interface Scores {
  home: number;
  away: number;
}

export interface CompetitorsEntity {
  name: string;
  position: string;
}

export interface EventStatus {
  active: boolean;
  started: boolean;
  live: boolean;
  resulted: boolean;
  finished: boolean;
  cashoutable: boolean;
  displayable: boolean;
  suspended: boolean;
  requestabet: boolean;
}
